.PHONY: all build fmt lint test vet clean install 
TARGET := $(shell basename $(shell pwd))

all: clean build fmt lint test vet

build: tidy verify
	@echo "+ $@"
	@go build -o $(TARGET) .

tidy:
	@echo "+ $@"
	@go mod tidy

verify:
	@echo "+ $@"
	@go mod verify

get-lint:
	@echo "+ $@"
	@go get golang.org/x/lint/golint

fmt:
	@echo "+ $@"
	@gofmt -s -l . | tee /dev/stderr

lint:
	@echo "+ $@"
	# go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	@golangci-lint run

test: build fmt lint vet
	@echo "+ $@"
	@go test ./...

vet:
	@echo "+ $@"
	@go vet ./...

clean:
	@echo "+ $@"
	@rm -rf $(TARGET)

install:
	@echo "+ $@"
	@go install .

bump:
	@echo "+ $@"
	@./scripts/bump.sh
	@git diff

release:
	@echo "+ $@"
	@./scripts/release.sh > /dev/random 2>&1
	@ls -lh *.7z

update-vendor:
	@echo "+ $@"
	go get -u ./...

