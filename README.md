# wallabag-add-article

commandline utility to add an article to wallabag


## Install

* copy config.json-example as config.json and adjust values to your wallabag instance
* run wallabag-add-article


## Parameters

### Help text printed by the application

```
Usage: ./wallabag-add-article [OPTION]... URL
  -archive
    	move article to archive
  -config string
    	file name of config JSON file (default "config.json")
  -d	get debug output (implies verbose mode)
  -h	print help
  -help
    	print help
  -starred
    	mark article as starred
  -tags string
    	tags for the article. if tags are given here, defined tagging rules in wallabag are not going to be applied any more
  -title string
    	title for the article. overwrites auto-retrieved title for the article
  -v	print version
  -verbose
    	verbose mode
```


### Example calls to the application

The URL always has to be the latest argument:

* add a URL `wallabag-add-article https://codeberg.org`
* add a URL and favorite it `wallabag-add-article -starred https://codeberg.org`
* add a URL and archive and favorite it `wallabag-add-article -archive -starred https://codeberg.org`
* add a URL with predefined title and specific tags `wallabag-add-article -title "Strubbl on Github" -tags "google, search" https://codeberg.org`
* add a URL using specific config `wallabag-add-article -config "$HOME/.wallabag/config.json" https://codeberg.org`


## TODO

test if article exists and abort then. also add force mode to add same article multiple times


## Project Status
### Source Code
The source code of this project can be found at: https://codeberg.org/strubbl/wallabag-add-article

### Go Report Card

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Strubbl/wallabag-add-article)](https://goreportcard.com/report/gitlab.com/Strubbl/wallabag-add-article)


