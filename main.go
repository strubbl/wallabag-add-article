package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/Strubbl/wallabago/v9"
)

const version = "0.1"
const defaultConfigJSON = "config.json"

var debug = flag.Bool("d", false, "get debug output (implies verbose mode)")
var h = flag.Bool("h", false, "print help")
var help = flag.Bool("help", false, "print help")
var v = flag.Bool("v", false, "print version")
var verbose = flag.Bool("verbose", false, "verbose mode")

var configJSON = flag.String("config", defaultConfigJSON, "file name of config JSON file")
var urlToAdd = ""
var title = flag.String("title", "", "title for the article. overwrites auto-retrieved title for the article")
var tags = flag.String("tags", "", "tags for the article. if tags are given here, defined tagging rules in wallabag are not going to be applied any more")
var starred = flag.Bool("starred", false, "mark article as starred")
var archive = flag.Bool("archive", false, "move article to archive")

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: %s [OPTION]... URL\n", os.Args[0])
	fmt.Fprintf(os.Stderr, "Try '%s -help' for more information.\n", os.Args[0])
}

func helptext() {
	fmt.Fprintf(os.Stderr, "Usage: %s [OPTION]... URL\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Println("home page: <https://codeberg.org/strubbl/wallabag-add-article>")
}

func handleFlags() {
	flag.Parse()
	urlToAdd = strings.Join(flag.Args(), " ")
	if *debug && len(flag.Args()) > 0 {
		log.Printf("handleFlags: non-flag args=%v", urlToAdd)
	}
	if *h || *help {
		helptext()
		os.Exit(0)
	}

	// version first, because it directly exits here
	if *v {
		fmt.Printf("version %v\n", version)
		os.Exit(0)
	}
	// test verbose before debug because debug implies verbose
	if *verbose && !*debug {
		log.Printf("verbose mode")
	}
	if *debug {
		log.Printf("handleFlags: debug mode")
		// debug implies verbose
		*verbose = true
	}
}

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	handleFlags()

	// URL checks
	// check if URL is given
	if urlToAdd == "" {
		usage()
		os.Exit(1)
	}

	if *verbose {
		log.Printf("url=\"%v\"", urlToAdd)
	}

	// check if URL is parsable
	u, err := url.Parse(urlToAdd)
	if err != nil {
		if *debug {
			log.Println("err while parsing URL:")
		}
		log.Fatal(err)
	}
	// check if URL has scheme, host and path
	if *debug {
		log.Println("url.Scheme ", u.Scheme)
		log.Println("url.Host ", u.Host)
		log.Println("url.Path ", u.Path)
	}
	if u.Scheme == "" || u.Host == "" {
		log.Fatal("invalid URL")
	}

	// check for config
	if *verbose {
		log.Println("reading config", *configJSON)
	}

	err = wallabago.ReadConfig(*configJSON)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	s := btoi(*starred)
	a := btoi(*archive)
	if *debug {
		log.Println("tags:", *tags)
		log.Println("title:", *title)
		log.Println("starred:", s)
		log.Println("archive:", a)
	}
	err = wallabago.PostEntry(urlToAdd, *title, *tags, s, a)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
